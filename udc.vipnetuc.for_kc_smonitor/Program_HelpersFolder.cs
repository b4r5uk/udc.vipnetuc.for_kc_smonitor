﻿using System.IO;

namespace udc.vipnetuc.for_kc_smonitor
{
    partial class Program
    {
        private static bool IsFolderExistsAndWritable(string folderPath)
        {
            try
            {
                var ds = Directory.GetAccessControl(folderPath);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
