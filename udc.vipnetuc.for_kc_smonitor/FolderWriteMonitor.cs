﻿using NLog;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace udc.vipnetuc.for_kc_smonitor
{
    class FolderWriteMonitor
    {
        string _fromFolder, _toFolder;
        static FileSystemWatcher _watcher;
        Logger _logger;

        public FolderWriteMonitor(string fromFolder, string toFolder, Logger logger)
        {
            this._fromFolder = fromFolder;
            this._toFolder = toFolder;
            this._logger = logger;
        }
        internal void Start()
        {
            using (_watcher = new FileSystemWatcher(_fromFolder))
            {
                _watcher.Created += new FileSystemEventHandler(Created_eventhandler);
                _watcher.EnableRaisingEvents = true;

                // Wait for the user to quit the program.
                _logger.Info("Vipnet KCS monitor started.");
                _logger.Info("Press 'q' to quit monitor.");
                while (Console.Read() != 'q') ;
            }
        }

        void Created_eventhandler(object sender, FileSystemEventArgs e)
        {
            _logger.Info($"File {e.ChangeType}: {e.FullPath}");
            Task.Run(() => NewFile(e.ChangeType.ToString(), e.FullPath, e.Name));
        }

        async private void NewFile(string changeType, string fullPath, string name)
        {
            string dstFilePath = Path.Combine(_toFolder, name);
            
            await Task.Delay(1000 * 30);
            bool copied = false;
            try
            {
                while (File.Exists(fullPath) && !copied)
                {
                    if (!IsFileLocked(new FileInfo(fullPath)))
                    {
                        File.Copy(fullPath, dstFilePath);                        
                        copied = true;
                        _logger.Warn($"File {name} was copied to {dstFilePath}");
                    }
                    else
                    {
                        Thread.Sleep(1000*2);
                    }
                }
                if (!copied)
                {
                    _logger.Info($"File {name} has gone");
                }

            }
            catch (Exception ex)
            {
                _logger.Warn($"Problem during movement of file {dstFilePath}");
                _logger.Warn(ex.Message);
            }
        }

        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

    }
}
