﻿using System;
using System.Configuration;
using NLog;

namespace udc.vipnetuc.for_kc_smonitor
{
    partial class Program
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            SetLogging();

            string _standardFromFolder = ConfigurationManager.AppSettings.Get("default_fromfolder");            
            string _standardToFolder = ConfigurationManager.AppSettings.Get("default_tofolder");
            string _fromFolder, _toFolder;

            if (args.Length == 2)
            {
                _fromFolder = args[0];
                _toFolder = args[1];
            }
            else
            {
                _fromFolder = _standardFromFolder;
                _toFolder = _standardToFolder;
            }
            if (!(IsFolderExistsAndWritable(_fromFolder) && IsFolderExistsAndWritable(_toFolder)))
            {
                Logger.Warn("Bad folder paths");
                Environment.Exit(0);
            }
            //monitor
            FolderWriteMonitor monitor = new FolderWriteMonitor(_fromFolder, _toFolder, Logger);
            monitor.Start();
        }

        private static void SetLogging()
        {
            var logconfig = new NLog.Config.LoggingConfiguration();
            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = "log.txt", ArchiveAboveSize=500000 };
            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            string _fromAddr= ConfigurationManager.AppSettings.Get("mail_fromaddress");
            string _toAddr = ConfigurationManager.AppSettings.Get("mail_adminaddress");
            string _smtpServer = ConfigurationManager.AppSettings.Get("mail_server");

            var log2mail = new NLog.Targets.MailTarget("log2mail") { From=_fromAddr, SmtpServer= _smtpServer, To= _toAddr };

            logconfig.AddRule(LogLevel.Info, LogLevel.Fatal, logconsole);
            logconfig.AddRule(LogLevel.Info, LogLevel.Fatal, logfile);
            logconfig.AddRule(LogLevel.Warn, LogLevel.Fatal, log2mail);

            NLog.LogManager.Configuration = logconfig;
        }
    }
}
